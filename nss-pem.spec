Name:       	nss-pem
Version:    	1.0.6
Release:    	3
Summary:    	PEM file reader for Network Security Services (NSS)
License:    	MPLv1.1
URL:        	https://github.com/kdudka/nss-pem
Source0:    	https://github.com/kdudka/nss-pem/releases/download/%{name}-%{version}/%{name}-%{version}.tar.xz

BuildRequires: 	cmake gcc nss-pkcs11-devel
Requires: 	nss%{?_isa} >= %(nss-config --version 2>/dev/null || echo 0)
Conflicts: 	nss%{?_isa} < 3.24.0-3%{?dist}
Obsoletes: 	%{name} < %{version}-%{release}

%description
PEM file reader for Network Security Services (NSS), implemented as a PKCS#11
module.

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="%{optflags} -DNSS_PKCS11_2_0_COMPAT"
mkdir build
pushd build
%cmake ../src
make %{?_smp_mflags} VERBOSE=yes
popd

%install
pushd build
%make_install
popd

%check
pushd build
ctest %{?_smp_mflags} --output-on-failure
popd

%files
%{_libdir}/libnsspem.so
%license COPYING

%changelog
* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.0.6-3
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Aug 7 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 1.0.6-2
- Add -DNSS_PKCS11_2_0_COMPAT to solve build failure

* Tue Jul 28 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 1.0.6-1
- update version to 1.0.6

* Thu Aug 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.4-2
- Package init
